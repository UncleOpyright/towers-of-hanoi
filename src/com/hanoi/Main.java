package com.hanoi;

import java.util.Scanner;

/**
 * Title: Towers of Hanoi
 * @author Sulla Deochsson
 * Date: 2019-01-08
 * Description: A short implementation of the Towers of Hanoi game, focusing on the Computerized solving of it
 */
public class Main {
    //Copy me, I want to travel
    public static void main(String[] args){
        //Declarables
        Scanner FBI = new Scanner(System.in);
        int in, in2;


        //Start with a pre-routine
        System.out.println("Towers of Hanoi");
        System.out.print("by Sulla Deochsson, 2019-01-08");
        System.out.println();
        System.out.print("How many disks would you like on the first peg? ");

        //Create the gameboard

        HanoiGame gameBoard;
        try {
            in = FBI.nextInt();
            gameBoard = new HanoiGame(in);
        }catch(NumberFormatException e){
            System.out.println("Invalid entry. Defaulting to 6 pegs");
            gameBoard = new HanoiGame(6);
        }

        System.out.println("Would you like to solve it yourself, or have a computer do it?");
        System.out.println("(1 for yourself, 2 for the computer)");
        in = FBI.nextInt();

            //Human Solve
        if(in == 1){
            boolean quit = false;
            while(!quit){
                gameBoard.printStacks();
                System.out.println("What peg do you want to remove a disk from? (9 to quit)");
                in = FBI.nextInt();
                if(in == 9){
                    quit = true;
                }else {
                    System.out.println("What peg do you want to move to?");
                    in2 = FBI.nextInt();
                    gameBoard.moveDisk(in, in2);
                }
                //TODO: Win detection
                //GUI
            }

            //Robosolve
        } else if (in == 2) {
            gameBoard.setVerbosity(true);
            System.out.println("How many seconds of delay do you wish to have between moves");
            in = FBI.nextInt();
            gameBoard.setDelay((long) in);
            gameBoard.solve();


        }else{
            System.out.println("Invalid Entry. Quitting");

        }
        FBI.close();
    }








}


