package com.hanoi;

/**
 * @author Sulla Deochsson
 * Date: 2019-01-08
 * Description: A library for the Towers of Hanoi game
 */

import java.util.Stack;
import java.util.concurrent.TimeUnit;

public class HanoiGame {
    //Provincial Variables
     private Stack<Disk> peg1 = new Stack<>();
     private Stack<Disk> peg2 = new Stack<>();
     private Stack<Disk> peg3 = new Stack<>();
     private int disks;
     private boolean doVerbosity = false;
     private long timeDelay = 0;

    /**
     * Create a game of Tower of Hanoi
     * @param disks How many disks should exist?
     */
    HanoiGame(int disks){
        Disk bluray;
        int sizeInt = disks;
        this.disks = disks;
        for(int i = 0; i < disks; i++) {
            bluray = new Disk(sizeInt);
            peg1.push(bluray);
            sizeInt--;
        }


    }

    /**
     * Move a disk from one stack to another
     * @param pegFrom Number of the peg to take a disk off of
     * @param pegTo Number of the peg to place a disk on
     */
    public void moveDisk(int pegFrom, int pegTo){
        Disk tempDisk;


            switch (pegFrom) {
                case 1:
                    switch(pegTo){
                        case 2:
                            //Error catching, yay!
                            if((peg1.size() == 0) || !(peg2.size() == 0) && peg2.peek().size < peg1.peek().size){
                                //If origin is empty or peg 2's top disk size is bigger than peg 1's size
                                System.out.println("Invalid Move");
                                break;
                            }
                            tempDisk = peg1.pop();
                            peg2.push(tempDisk);
                            break;
                        case 3:
                            if((peg1.size() == 0) || !(peg3.size() == 0) && peg3.peek().size < peg1.peek().size){
                                System.out.println("Invalid Move");
                                break;
                            }//else {
                            tempDisk = peg1.pop();
                            peg3.push(tempDisk);
                            break;
                            //}
                        default:
                            System.out.println("Invalid Move");
                            break;
                    }
                    break;
                case 2:
                    switch(pegTo){
                        case 1:
                            if((peg2.size() == 0) || !peg1.isEmpty() && peg1.peek().size < peg2.peek().size){
                                System.out.println("Invalid Move");
                                break;
                            }
                            tempDisk = peg2.pop();
                            peg1.push(tempDisk);
                            break;
                        case 3:
                            if((peg2.size() == 0) || !peg3.isEmpty() && peg3.peek().size < peg2.peek().size){
                                System.out.println("Invalid Move");
                                break;
                            }
                            tempDisk = peg2.pop();
                            peg3.push(tempDisk);
                            break;
                        default:
                            System.out.println("Invalid Move");
                            break;
                    }
                    break;
                case 3:
                    switch(pegTo){
                        case 1:
                            if((peg3.size() == 0) || !peg1.isEmpty() && peg1.peek().size < peg3.peek().size){
                                System.out.println("Invalid Move");
                                break;
                            }
                            tempDisk = peg3.pop();
                            peg1.push(tempDisk);
                            break;
                        case 2:
                            if((peg3.size() == 0) || !peg2.isEmpty() && peg2.peek().size < peg3.peek().size){
                                System.out.println("Invalid Move");
                                break;
                            }
                            tempDisk = peg3.pop();
                            peg2.push(tempDisk);
                            break;
                        default:
                            System.out.println("Invalid Move");
                            break;
                    }
                    break;
                default:
                    System.out.println("The peg does not exist");

            }
            if(doVerbosity) printStacks();
        try {
            TimeUnit.SECONDS.sleep(timeDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * Print the disks currently on the stacks
     *
     */
    public void printStacks() {

        System.out.println("Stack 1:");
        for(int i = (peg1.size() - 1); i > -1; i--){
            System.out.println(peg1.elementAt(i).size);
        }

        System.out.println("Stack 2:");
        for(int i = (peg2.size() - 1); i > -1; i--){
            System.out.println(peg2.elementAt(i).size);
        }

        System.out.println("Stack 3:");
        for(int i = (peg3.size() - 1); i > -1; i--){
            System.out.println(peg3.elementAt(i).size);
        }

        System.out.println();


    }

    /**
     * Public rendition of solve
     */
    public void solve(){
        if(doVerbosity) printStacks();
        solve(disks, 1, 3);
        if(!doVerbosity) printStacks(); //Avoid an extra print

    }



    /**
     * This method solves the board gameboard
     * It is private since it has to call itself, and allowing the user to place in variables could lead to bad things
     * @param levels how many levels to solve(important internally
     * @param from which peg to go take disks from
     * @param to which peg we are solving to
     */
    private void solve(int levels, int from, int to){
        int mid = 0;
        //Start by figuring out what the middle peg is
        switch(from){
            case 1:
                switch(to) {
                    case 2:
                        mid = 3;
                        break;
                    case 3:
                        mid = 2;
                        break;

                }
                break;
            case 2:
                switch(to){
                    case 1:
                        mid = 3;
                        break;
                    case 3:
                        mid = 1;
                        break;
                }
                break;
            case 3:
                switch(to){
                    case 1:
                        mid = 2;
                        break;
                    case 2:
                        mid = 1;
                        break;
                }
                break;
        }



        //Levels = 1, only a single disk move is needed. Then be done
        if(levels == 1) {
            this.moveDisk(from, to);

        }else{
            int j = 2;
            if((levels % 2) == 0){ //EVEN # OF DISKS
                moveDisk(from, mid);
                for(int i = 1; i < levels; i++) {

                    moveDisk(from, to);
                    solve(i, mid, to);
                    if(levels == j) break;
                    i++; j++;
                    moveDisk(from, mid);
                    solve(i, to, mid);
                    if(levels == j) break;
                    j++;
                }
            }else{ //ODD # OF DISKS
                moveDisk(from, to);
                for(int i = 1; i < levels; i++){
                    moveDisk(from, mid);
                    solve(i, to, mid);
                    if(levels == j) break;
                    i++; j++;
                    moveDisk(from, to);
                    solve(i, mid, to);
                    if(levels == j) break;
                    j++;
                }
            }
            /*
             * The algorithm essentially works like this:
             * Step 1: Determine the middle peg
             * Step 2: Move a disk from the origin to either the middle or the end (if even or odd, respectively)
             * Step 3: Move another disk to the other peg
             * Step 4: Solve onto the peg
             * Step 5: Move disk onto another peg
             * Step 6: Solve onto the peg
             * Step 7: Rinse & repeat until fully solved
             */


        }










    }

    /**
     * Are we talkin or not?
     * @param v Are we talkin or not?
     */
    public void setVerbosity(boolean v){
        this.doVerbosity = v;
    }

    /**
     * S
     * @param i
     */
    public void setDelay(long i){
        this.timeDelay = i;
    }
}
